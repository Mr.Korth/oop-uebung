package tuc.isse.uebung07;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class Shop {

	protected List<Item> items = new Vector<Item>();
	protected List<String> customerNames = new Vector<String>();
	protected Map<String, List<Integer>> cartItems = new HashMap<String, List<Integer>>();
	

	public String getItemName(int itemIndex) {
		return items.get(itemIndex).getName();
	}

	public void setItemName(int itemIndex, String newName) {
		items.get(itemIndex).setName(newName);
	}

	public int getItemPrice(int itemIndex) {
		return items.get(itemIndex).getPrice();
	}

	public void setItemPrice(int itemIndex, int newPrice) {
		items.get(itemIndex).setPrice(newPrice);
	}

	public List<Integer> getCartItemsList(String name) {
		return cartItems.get(name);
	}

	public void setCartItems(Map<String, List<Integer>> cartItems) {
		this.cartItems = cartItems;
	}

	public Integer addItem(String name, int price) {
		items.add(new Item(name, price));
		return items.size();
	}

	public void printItem(int itemIndex) {
		System.out.println("item " + itemIndex + ":" + items.get(itemIndex).printItem());
	}

	public void addCustomer(String name) {
		customerNames.add(name);
		cartItems.put(name, new Vector<Integer>());
	}

	public void removeCustomer(String name) {
		customerNames.remove(name);
		cartItems.remove(name);
	}

	public void printCustomer() {
		for (String name : customerNames) {
			System.out.println("customer:" + name);
		}
	}

	public void addItemToCart(String name, Integer itemIndex) {
		cartItems.get(name).add(itemIndex);
	}
	
}
