package tuc.isse.uebung07;

import java.util.List;
import java.util.Vector;

public class WebShop extends Shop{

	protected List<String> urls = new Vector<String>();
	
	public void addURL(String url) {
		urls.add(url);
	}
	
	public void removeURL(String url) {
		urls.remove(url);
	}

	public void printURL() {
		for (String url : urls) {
			System.out.println(url);
		}
	}
	
}
