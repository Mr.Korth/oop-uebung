package tuc.isse.uebung07;

import java.util.List;

public class Kasse {

	public int cartSum(WebShop shop, String customerName) {
		List<Integer> cart = shop.getCartItemsList(customerName);
		Integer sum = 0;
		// test
		for (int i = 0; i < cart.size(); i++) {
			sum += shop.getItemPrice(cart.get(i));
		}
		return sum;
	}

	public Integer countItems(WebShop shop, String customerName) {
		List<Integer> cart = shop.getCartItemsList(customerName);
		return cart.size();
	}

	public Integer countItems(WebShop shop, String customerName, Integer itemID) {
		List<Integer> cart = shop.getCartItemsList(customerName);

		Integer count = 0;
		for (Integer itemInCart : cart) {
			count += this.checkItemInCart(itemID, itemInCart);

		}

		return count;
	}

	public int checkItemInCart(Integer itemID, Integer itemInCartId) {
		if (itemID == itemInCartId) {
			return 1;
		} else {
			return 0;
		}
		
	}
}
